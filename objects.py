class Skill():
    name = None
    confirmation_count = None

    def __init__(self, name=None, confirmation_count=None):
        self.name = name
        self.confirmation_count = confirmation_count
    def __repr__(self):

        return "{name} {confirmation_count}".format(name=self.name, confirmation_count=self.confirmation_count)

class Education():

    from_date = None
    to_date = None
    description = None
    degree = None

    def __init__(self, from_date=None, to_date=None, description=None, degree=None):
        self.from_date = from_date
        self.to_date = to_date
        self.description = description
        self.degree = degree

    def __repr__(self):
        return "{degree} at {company} from {from_date} to {to_date}".format(
        from_date = self.from_date, to_date = self.to_date, degree = self.degree, company = self.institution_name)

class Position():

    position_name = None
    company = None
    date_range = None
    duration = None
    location = None
    description = None

    def __init__(self, position_name = None, company=None, date_range=None, duration = None, location = None, description = None):

        self.position_name = position_name
        self.company = company
        self.date_range = date_range
        self.duration = duration
        self.location = location
        self.description = description


class Message():

    profile_uuid = None
    profile_login = None
    person_uuid = None
    person_name = None
    person_url = None
    message_text = None
    message_datetime = None
    direction = None

    def __init__(self, profile_uuid = None, profile_login=None, person_uuid = None, person_name = None, person_url = None, message_text = None):
        self.profile_uuid = profile_uuid
        self.profile_login = profile_login
        self.person_uuid = person_uuid
        self.person_name = person_name
        self.person_url = person_url
        self.message_text = message_text

    def sent_message(driver):



        print('Sent')

class Event():

    profile_uuid = None
    profile_login = None
    IP = None
    action_datetime = None
    action_type = None
    action_subtype = None
    action_link = None
    action_element = None
    error_message = None

    def __init__(self, profile_uuid = None, profile_login = None, action_datetime = None,
                action_type = None, action_subtype = None, action_link = None, action_element = None, error_message = None):
        self.profile_uuid = profile_uuid
        self.profile_login = profile_login
        self.action_datetime = action_datetime
        self.action_type = action_type
        self.action_subtype = action_subtype
        self.action_link = action_link
        self.action_element = action_element
        self.error_message = error_message


class Invitation():

    inviter_uuid = None
    inviter_login = None
    invited_person_uuid = None
    invited_person_url = None
    invitation_send_datetime = None
    last_update_datetime = None
    is_sent = False
    is_accepted = False
    acceptation_datetime = None

    def __init__(self, inviter_uuid = None, inviter_login = None, invited_person_uuid = None,
    invited_person_url= None, invitation_send_datetime = None):

        self.inviter_uuid = inviter_uuid
        self.inviter_login = inviter_login
        self.invited_person_uuid = invited_person_uuid
        self.invited_person_url = invited_person_url
        self.invitation_send_datetime = invitation_send_datetime

    def send_invitation():
        print('invitation_sent')
