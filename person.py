import objects
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import uuid
import unicodedata
import time


class Person():
    driver = None
    name = None
    position = None
    company = None
    location = None
    uuid = None
    educations = []
    experience = []
    skills = []
    linkedin_url = None
    scrape = None
    SCROLL_PAUSE_TIME = None

    def __init__(self, linkedin_url = None, name = None, educations = [], experience = [], skills = [], driver = None, get = True):
        self.linkedin_url = linkedin_url
        self.name = name
        self.educations = educations or []
        self.experience = experience or []
        self.skills = skills or []
        self.uuid = uuid.uuid4().hex

        if get:
            driver.get(linkedin_url)
            ### scrolling
            self.SCROLL_PAUSE_TIME = 1

            # Get scroll height
            last_height = driver.execute_script("return document.body.scrollHeight")

            while True:
                            # Scroll down to bottom
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                            # Wait to load page
                time.sleep(self.SCROLL_PAUSE_TIME)

                            # Calculate new scroll height and compare with last scroll height
                new_height = driver.execute_script("return document.body.scrollHeight")
                if new_height == last_height:
                    break
                last_height = new_height
        self.driver = driver

    def add_skill(self, skill):
        self.skills.append(skill)

    def add_education(self, education):
        self.educations.append(education)

    def add_experience(self, position):
        self.experience.append(position)

    def get_basic_info(self):
        try:
            self.driver.execute_script("window.scrollTo(0, 0);")
            time.sleep(self.SCROLL_PAUSE_TIME)
            _ = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "pv-top-card-v2-section__company-name")))
            self.company = self.driver.find_element_by_class_name('pv-top-card-v2-section__company-name').text
        except:
            print('no company name for: ' + self.linkedin_url)
        try:
            _ = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "pv-top-card-section__name")))
            self.name = self.driver.find_element_by_class_name('pv-top-card-section__name').text
        except:
            try:
                self.name = self.driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "break-words", " " ))]').text
            except:
                print('no name for: ' + self.linkedin_url)

        try:
            _ = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "pv-top-card-section__headline")))
            self.position = self.driver.find_element_by_class_name('pv-top-card-section__headline').text
        except:
            try:
                self.position = self.driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "t-18", " " ))]').text
            except:
                print('no position for: ' + self.linkedin_url)
        try:
            _ = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "pv-top-card-section__location")))
            self.location = self.driver.find_element_by_class_name('pv-top-card-section__location').text
        except:
            try:
                self.location = self.driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "t-black", " " )) and contains(concat( " ", @class, " " ), concat( " ", "inline-block", " " ))]').text
            except:
                print('no location for: ' + self.linkedin_url)


    def get_educations(self):
        try:
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/4));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/2));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight*3/4));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            edu = self.driver.find_element_by_id("education-section")
            for school in edu.find_elements_by_class_name("pv-profile-section__sortable-item"):
                university = school.find_element_by_class_name("pv-entity__school-name").text
                degree = None
                try:
                    degree = school.find_element_by_class_name("pv-entity__comma-item").text
                except:
                    degree = None
                education = objects.Education(degree=unicodedata.normalize('NFD', degree).encode('ascii', 'ignore'))
                education.institution_name = unicodedata.normalize('NFD', university).encode('ascii', 'ignore')
                self.add_education(education)
        except:
            print('no edu for profile:' + self.linkedin_url)

    def get_skills(self):
        try:
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/4));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/2));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight*3/4));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight));")
            time.sleep(self.SCROLL_PAUSE_TIME)
            _ = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "pv-skills-section__additional-skills")))
            element_show_more = self.driver.find_element_by_class_name('pv-skills-section__additional-skills')
            element_show_more.location_once_scrolled_into_view
            element_show_more.click()
        except:
            try:
                self.driver.execute_script("arguments[0].click();", element_show_more)
            except:
                print('no skill element show more for profile:' + self.linkedin_url)

        skills_box = self.driver.find_elements_by_class_name('pv-skill-category-entity__skill-wrapper')
        for i in skills_box:
            name = i.find_element_by_class_name('pv-skill-category-entity__name').text
            name = unicodedata.normalize('NFD', name).encode('ascii', 'ignore')
            try:
                count = i.find_element_by_class_name('pv-skill-category-entity__endorsement-count').text
            except:
                count = 0
            skill = objects.Skill(name, count)
            self.add_skill(skill)

    def get_experience(self):
        try:
            position_entities = self.driver.find_elements_by_class_name("pv-position-entity")
        except:
            print('no position entieties')

        for entity in position_entities:
            role_details_container = entity.find_elements_by_class_name('pv-entity__role-details-container')

            if len(role_details_container) > 0:
                for role in role_details_container:
                    try:
                        show_more = role.find_element_by_class_name("lt-line-clamp__more")
                        self.driver.execute_script("arguments[0].click();", show_more)
                    except:
                        print("EXPERIENCE: no show more")
                    try:
                        position_name = role.find_element_by_class_name("t-bold").text
                        position_name = unicodedata.normalize('NFD', position_name).encode('ascii', 'ignore')
                    except:
                        position_name = None
                        print("EXPERIENCE: no position name")
                    try:
                        company = entity.find_element_by_class_name('t-bold').text
                        company = unicodedata.normalize('NFD', company).encode('ascii', 'ignore')
                    except:
                        company = None
                        print("EXPERIENCE: No company")
                    try:
                        date_range = role.find_element_by_class_name("pv-entity__date-range").text
                        date_range = unicodedata.normalize('NFD', date_range).encode('ascii', 'ignore')
                    except:
                        date_range = None
                        print("EXPERIENCE: no date_range")
                    try:
                        duration = role.find_element_by_class_name("pv-entity__bullet-item-v2").text
                        duration = unicodedata.normalize('NFD', duration).encode('ascii', 'ignore')
                    except:
                        duration = None
                        print("EXPERIENCE: no duration")
                    try:
                        location = role.find_element_by_class_name("pv-entity__location").text
                        location = unicodedata.normalize('NFD', location).encode('ascii', 'ignore')
                    except:
                        location = None
                        print("EXPERIENCE: No location")
                    try:
                        description = role.find_element_by_class_name("pv-entity__description").text
                        description = unicodedata.normalize('NFD', description).encode('ascii', 'ignore')
                    except:
                        description = None
                        print("EXPERIENCE: no description")

                    position = objects.Position(position_name, company, date_range, duration, location, description)
                    self.add_experience(position)
            else:
                try:
                    show_more = entity.find_element_by_class_name("lt-line-clamp__more")
                    self.driver.execute_script("arguments[0].click();", show_more)
                except:
                    print("EXPERIENCE: no show more")
                try:
                    position_name = entity.find_element_by_class_name("t-bold").text
                    position_name = unicodedata.normalize('NFD', position_name).encode('ascii', 'ignore')
                except:
                    position_name = None
                    print("EXPERIENCE: no position name")
                try:
                    company = entity.find_element_by_class_name("pv-entity__secondary-title").text
                    company = unicodedata.normalize('NFD', company).encode('ascii', 'ignore')
                except:
                    company = None
                    print("EXPERIENCE: No company")
                try:
                    date_range = entity.find_element_by_class_name("pv-entity__date-range").text
                    date_range = unicodedata.normalize('NFD', date_range).encode('ascii', 'ignore')
                except:
                    date_range = None
                    print("EXPERIENCE: no date_range")
                try:
                    duration = entity.find_element_by_class_name("pv-entity__bullet-item-v2").text
                    duration = unicodedata.normalize('NFD', duration).encode('ascii', 'ignore')
                except:
                    duration = None
                    print("EXPERIENCE: no duration")
                try:
                    location = entity.find_element_by_class_name("pv-entity__location").text
                    location = unicodedata.normalize('NFD', location).encode('ascii', 'ignore')
                except:
                    location = None
                    print("EXPERIENCE: No location")
                try:
                    description = entity.find_element_by_class_name("pv-entity__description").text
                    description = unicodedata.normalize('NFD', description).encode('ascii', 'ignore')
                except:
                    description = None
                    print("EXPERIENCE: no description")

                position = objects.Position(position_name, company, date_range, duration, location, description)
                self.add_experience(position)
