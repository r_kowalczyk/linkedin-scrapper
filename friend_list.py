from selenium import webdriver
import selenium.webdriver.chrome.service as service
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import pandas as pd
import re
import person
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
from sqlalchemy import create_engine
from selenium.webdriver.common.keys import Keys
import time
import mysql.connector

mydb = mysql.connector.connect(
  host="77.55.217.154",
  user="jerry",
  passwd="eloelo320",
  database="linkedin")

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM linkedin_accounts")
linkedin_accounts = pd.DataFrame(mycursor.fetchall())

linkedin_accounts.columns = mycursor.column_names

linkedin_accounts

friend_list_url  = 'https://www.linkedin.com/mynetwork/invite-connect/connections/'

login_url = "https://www.linkedin.com/login"
login =linkedin_accounts["login"][2]
password = linkedin_accounts["password"][2]

webdriver_path = '/home/rafal/Downloads/chromedriver'

driver = webdriver.Chrome(webdriver_path)


driver.get(login_url)

driver.find_element_by_id("username").send_keys(login)
driver.find_element_by_id("password").send_keys(password)


driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "from__button--floating", " " ))]').click()


driver.get(friend_list_url)

friends_number = driver.find_element_by_class_name('mn-connections__header').text
friends_number = re.sub("[^0-9]", "", friends_number)

friends_list = driver.find_elements_by_class_name("mn-connection-card__details")

while len(friends_list) != int(friends_number):
    driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight));")
    friends_list = driver.find_elements_by_class_name("mn-connection-card__details")


friends_list_urls = []

for j in friends_list:
    url = j.find_element_by_css_selector(".mn-connection-card__link.ember-view").get_attribute('href')
    friends_list_urls.append(url)


### get friends

friends = []


for i in friends_list_urls:

    per = person.Person(linkedin_url = i, driver = driver, get = True)
    per.get_basic_info()
    per.get_educations()
    per.get_skills()
    friends.append(per)



messaging_url = 'https://www.linkedin.com/messaging/'

driver.get(messaging_url)

messages = []

for p in friends:
    #p = friends[1]
    name = p.name
    driver.find_element_by_id("search-conversations").clear()
    driver.find_element_by_id("search-conversations").send_keys(name)
    time.sleep(2)
    driver.find_element_by_id("search-conversations").send_keys(Keys.ENTER)
    try:
        _ = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "msg-conversation-card__content")))
        conversations = driver.find_elements_by_class_name('msg-conversation-card__content')
        if(len(conversations) >= 1):
            try:
                _ = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "msg-s-event-listitem__body")))
                body = driver.find_element_by_class_name('msg-s-event-listitem__body').text
                message = [name, body]
                messages.append(message)
            except:
                print('n')
    except:
        print('n')




len(messages)

messages
