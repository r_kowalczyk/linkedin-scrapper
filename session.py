import search
import time
from selenium import webdriver
import selenium.webdriver.chrome.service as service
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from datetime import datetime
import pymysql
import uuid
pymysql.install_as_MySQLdb()
import MySQLdb
from sqlalchemy import create_engine
import profile
import pandas as pd
from pandas.io import sql

class Session():

    session_uuid = None
    datetime_start = None
    end_datetime = None
    profile_base = None
    profile_uuid = None
    events = []
    searches = []
    driver = None
    webdriver_path = '/home/rafal/Downloads/chromedriver'
    db_driver = None

    def __init__(self, profile_number = None, login = None, password = None, ip = None, database = None, cookie = None):

        self.datetime_start = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        self.driver = webdriver.Chrome(self.webdriver_path)
        if(not (cookie is None)):
            self.driver.add_cookie({
                    'name': 'li_at',
                    'value': cookie,
                    'domain': '.linkedin.com'
                })
        #self.connect_db_driver(login, password, ip, database)
        self.db_driver = create_engine("mysql+mysqldb://"+login+":"+password+"@"+ip+"/"+database)
        self.session_uuid = uuid.uuid4().hex
        #self.profile_base = profile.Profile(login, password, ip, database, profile_number)



    def log_profile(self, number = 1):

        #df = pd.DataFrame(self.db_driver.execute('select * from linkedin_accounts').fetchall())
        #prof = df.iloc[[2]]
        #uuid = prof[[3]]
        #self.profile_uuid = uuid
        #password = prof[[2]]
        #login = prof[[1]]

        login_url = "https://www.linkedin.com/login"

        self.profile_uuid = '2532d865564f442aa9718a16cef2e3a5'
        self.driver.get(login_url)
        time.sleep(2)
        self.driver.find_element_by_id("username").send_keys("kleofaszaruba@wp.pl")
        time.sleep(2)
        self.driver.find_element_by_id("password").send_keys("rekruter1")
        time.sleep(2)
        self.driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "from__button--floating", " " ))]').click()

    def connect_db_driver(self, login=None, password = None, ip = None, database = None):

        self.db_driver = create_engine("mysql+mysqldb://"+login+":"+password+"@"+ip+"/"+database)

    def session(self):

        session_df = pd.DataFrame([[self.session_uuid, self.datetime_start, self.profile_uuid]], columns = ['session_uuid', 'datetime_start', 'profile_uuid'])
        sql.to_sql(session_df, con=self.db_driver, name='sessions',if_exists='append', index = False)

        ANDs = ["warszawa", "java", "groovy"]

        search_2 = search.Search(ANDs =ANDs, driver = self.driver, session = self)
        self.searches.append(search_2)
        search_2.search_and_scrape()
        self.end_datetime = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
