import time
import pandas as pd
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
from sqlalchemy import create_engine
import profile

class Profile():

    uuid = None
    login = None
    password = None
    last_login = None
    number_of_searches = None

    def __init__(self, login = None, password = None, ip = None, database = None, profile_number = None):

        db_driver = create_engine("mysql+mysqldb://"+login+":"+password+"@"+ip+"/"+database)
        df = pd.DataFrame(db_driver.execute('select * from linkedin_accounts').fetchall())
        df = pd.DataFrame(data =df, columns = db_driver.execute('select * from linkedin_accounts').keys())
        prof = df.iloc[[profile_number]]
        self.uuid = prof[[3]]
        self.password = prof[[2]]
        self.login = prof[[1]]

    def log_in(self, driver):

        login_url = "https://www.linkedin.com/login"

        driver.get(login_url)
        time.sleep(2)
        driver.find_element_by_id("username").send_keys(self.login)
        time.sleep(2)
        driver.find_element_by_id("password").send_keys(self.password)
        time.sleep(2)
        driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "from__button--floating", " " ))]').click()

    def add_profile(self, login, password):
        print('add profile - no function yet')
