import person
import re
import math
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
from sqlalchemy import create_engine
import uuid
import numpy as np
import pandas as pd
from pandas.io import sql
from datetime import datetime
import unicodedata
import time

class Search():


    persons = []
    ANDs = []
    NOTs = []
    search_query = 'rekruter it director engineering risk marketing'
    driver = None
    session = None
    db_driver = None
    db_urls = []

    def __init__(self, search_query = None, ANDs = [], NOTs = [], driver = None, session = None):

        self.ANDs = ANDs
        self.NOTs = NOTs

        if(search_query is None):
            for i in range(len(ANDs)):
                if(i != 0):
                    self.search_query = self.search_query + " AND " + ANDs[i]
                else:
                    self.search_query = ANDs[i]
            for i in range(len(NOTs)):
                self.search_query = self.search_query + " AND NOT " + NOTs[i]
        else:
            self.search_query = search_query

        self.driver = driver
        self.session = session
        self.db_driver = create_engine("mysql+mysqldb://jerry:eloelo320@77.55.217.154/linkedin")
        self.db_urls = list(self.db_driver.execute("select linkedin_url from person_info").fetchall())
        self.db_urls = [ x[0] for x in self.db_urls]

    def search_and_scrape(self):

        ### add search to db
        for i in range(len(self.ANDs)):
            search_df = pd.DataFrame([[self.session.session_uuid, self.ANDs[i], "AND"]], columns = ['session_uuid', 'value', 'logical_operator'])
            sql.to_sql(search_df, con=self.db_driver, name='searches',if_exists='append', index = False)
        for i in range(len(self.NOTs)):
            search_df = pd.DataFrame([[self.session.session_uuid, self.NOTs[i], "AND NOT"]], columns = ['session_uuid', 'value', 'logical_operator'])
            sql.to_sql(search_df, con=self.db_driver, name='searches',if_exists='append', index = False)

        search_url = 'https://www.linkedin.com/search/results/people/?keywords=' + self.search_query
        self.driver.get(search_url)

        calkowita_liczba_wynikow = self.driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "clear-both", " " ))]').text
        calkowita_liczba_wynikow = re.sub("[^0-9]", "", calkowita_liczba_wynikow)
        maksymalna_liczba_stron_wyszukiwania = math.ceil(int(calkowita_liczba_wynikow)/10)

        for j in range(maksymalna_liczba_stron_wyszukiwania):

            page_number = j + 1
            search_url = 'https://www.linkedin.com/search/results/people/?keywords=' + self.search_query + '&page=' + str(page_number)
            self.driver.get(search_url)
            # wyniki = driver.find_elements_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "search-result__result-link ember-view", " " ))]')
            self.driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/2));")


            _ = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "search-result__result-link")))
            wyniki = self.driver.find_elements_by_class_name('search-result__result-link')
            if(len(wyniki) == 0):
                print('Przerwania wyszukiwania, brak wynikow: ' + search_url)
                break

            urls = []

            for i in wyniki:
                urls.append(i.get_attribute('href'))

            urls = list(set(urls))
            final_urls = []

            for url in urls:
                if url not in self.db_urls:
                    final_urls.append(url)

            final_urls = list(set(final_urls))

            for i in final_urls:

                per = person.Person(linkedin_url=i, driver=self.driver, get=True)
                per.get_basic_info()
                per.get_educations()
                per.get_skills()
                per.get_experience()
                self.persons.append(per)

                person_educations = pd.DataFrame(columns = ['uuid', 'degree', 'institution_name'])
                for j in range(len(per.educations)):
                    education = per.educations[j]
                    person_education = pd.DataFrame([[per.uuid, education.degree, education.institution_name]], columns = ['uuid', 'degree', 'institution_name'])
                    person_educations = person_educations.append(person_education, ignore_index = True)

                sql.to_sql(person_educations, con=self.db_driver, name='educations',if_exists='append', index = False)

                person_skills = pd.DataFrame(columns = ['uuid', 'skill', 'confirmation_count'])
                for j in range(len(per.skills)):
                    skill = per.skills[j]
                    person_skill = pd.DataFrame([[per.uuid, skill.name, skill.confirmation_count]], columns = ['uuid', 'skill', 'confirmation_count'])
                    person_skills = person_skills.append(person_skill, ignore_index=True)

                sql.to_sql(person_skills, con=self.db_driver, name='skills',if_exists='append', index = False)

                person_experience = pd.DataFrame(columns = ['person_uuid', 'position_name', 'company', 'date_range', 'duration', 'location',  'description' ])
                for j in range(len(per.experience)):
                    position = per.experience[j]
                    person_position = pd.DataFrame([[per.uuid, position.position_name, position.company, position.date_range, position.duration, position.location, position.description]], columns = ['person_uuid', 'position_name', 'company', 'date_range', 'duration', 'location',  'description' ])
                    person_experience = person_experience.append(person_position, ignore_index=True)

                sql.to_sql(person_experience, con=self.db_driver, name='experience', if_exists='append', index=False)

                datetime_ = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
                uuid = per.uuid
                if per.name is None:
                    per.name = ""
                name = unicodedata.normalize('NFD', per.name).encode('ascii', 'ignore')
                linkedin_url = per.linkedin_url
                if per.position is None:
                    per.position = ""
                position = unicodedata.normalize('NFD', per.position).encode('ascii', 'ignore')
                if per.company is None:
                    per.company = ""
                company = unicodedata.normalize('NFD', per.company).encode('ascii', 'ignore')
                if per.location is None:
                    per.location = ""
                location = unicodedata.normalize('NFD', per.location).encode('ascii', 'ignore')

                person_info = pd.DataFrame([[datetime_, uuid, name, linkedin_url, position, company, location, self.session.session_uuid]], columns = ['datetime','uuid', 'name', 'linkedin_url','position','company', 'location', 'session_uuid'])

                sql.to_sql(person_info, con=self.db_driver, name='person_info',if_exists='append', index = False)
