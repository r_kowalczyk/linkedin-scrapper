
import uuid
from datetime import datetime
from pandas.io import sql
import pandas as pd

class Event():

    @staticmethod
    def push_event(engine, session_uuid = None, profile_uuid = None, profile_login = None,
    action_type = None, action_subtype = None,
    action_input= None, action_link = None, action_element = None, error_message = None):

        event_uuid = uuid.uuid4().hex
        event_datetime = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

        event = [[event_uuid, session_uuid, event_datetime, profile_uuid, profile_login, action_type,
        action_subtype, action_input, action_link,
        action_element, error_message]]

        columns = ["event_uuid", "session_uuid", "event_datetime", "profile_uuid", "profile_login", "action_type",
        "action_subtype", "action_input", "action_link",
        "action_element", "error_message"]

        event_df = pd.DataFrame(data = event, columns = columns)

        sql.to_sql(event_df, con=engine, name='events',if_exists='append', index=False)

        return(True)
