import person
import re
import math
from selenium import webdriver
import selenium.webdriver.chrome.service as service
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import pandas as pd


############################################################################

# HASŁO DO WYSZUKIWANIA
search_key = 'rekruter it director engineering risk marketing'
# ŚCIEŻKA ZALEŻNA OD MASZYNY !!!!!
webdriver_path = '/home/rafal/Downloads/chromedriver'
# ŚCIEŻKA DO ZAPISU
save_path = '/home/rafal/Desktop/LinkedIn Scrapper/linkedin-scrapper/'

############################################################################

search_url = 'https://www.linkedin.com/search/results/people/?keywords=' + search_key
search_url
# launch url
url = "https://www.linkedin.com/login"

# create a new chrome session
driver = webdriver.Chrome(webdriver_path)

driver.get(url)

driver.find_element_by_id("username").send_keys("piotrmakowski2604@gmail.com")
driver.find_element_by_id("password").send_keys("eloelo")

driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "from__button--floating", " " ))]').click()

driver.get(search_url)

calkowita_liczba_wynikow = driver.find_element_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "clear-both", " " ))]').text
calkowita_liczba_wynikow = re.sub("[^0-9]", "", calkowita_liczba_wynikow)

maksymalna_liczba_stron_wyszukiwania = math.ceil(int(calkowita_liczba_wynikow)/10)
maksymalna_liczba_stron_wyszukiwania

persons = []

for j in range(maksymalna_liczba_stron_wyszukiwania):

    page_number = j + 1
    search_url = 'https://www.linkedin.com/search/results/people/?keywords=' + search_key + '&page=' + str(page_number)
    driver.get(search_url)
    # wyniki = driver.find_elements_by_xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "search-result__result-link ember-view", " " ))]')
    driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/2));")
    _ = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.CLASS_NAME, "search-result__result-link")))
    wyniki = driver.find_elements_by_class_name('search-result__result-link')
    if(len(wyniki) == 0):
        print('Przerwania wyszukiwania, brak wynikow: ' + search_url)
        break

    urls = []

    for i in wyniki:
        urls.append(i.get_attribute('href'))

    urls = list(set(urls))

    for i in urls:

        per = person.Person(linkedin_url=i, driver=driver, get=True)
        per.get_basic_info()
        per.get_educations()
        per.get_skills()
        persons.append(per)

# save data to df and csv
# no  ja wiem że te fory to można zoptymalizować i wrzucić wyżej ale tak jest czytelniej

persons_info_df = pd.DataFrame(columns=['uuid', 'name', 'linkedin_url', 'position', 'company', 'location'])

for k in range(len(persons)):
    person = persons[k]
    uuid = person.uuid
    name = person.name
    linkedin_url = person.linkedin_url
    position = person.position
    company = person.company
    location = person.location

    person_info = pd.DataFrame([[uuid, name, linkedin_url, position, company, location]], columns = ['uuid', 'name', 'linkedin_url','position','company', 'location'])
    persons_info_df = persons_info_df.append(person_info, ignore_index = True)


persons_educations_df = pd.DataFrame(columns = ['uuid', 'degree', 'institution_name'])

for i in range(len(persons)):
    person = persons[i]

    person_educations = pd.DataFrame(columns = ['uuid', 'degree', 'institution_name'])
    for j in range(len(person.educations)):
        education = person.educations[j]
        person_education = pd.DataFrame([[person.uuid, education.degree, education.institution_name]], columns = ['uuid', 'degree', 'institution_name'])
        person_educations = person_educations.append(person_education, ignore_index = True)

    persons_educations_df = persons_educations_df.append(person_educations, ignore_index = True)


persons_skills_df = pd.DataFrame(columns = ['uuid', 'skill', 'confirmation_count'])

for i in range(len(persons)):
    person = persons[i]

    person_skills = pd.DataFrame(columns = ['uuid', 'skill', 'confirmation_count'])
    for j in range(len(person.skills)):
        skill = person.skills[j]
        person_skill = pd.DataFrame([[person.uuid, skill.name, skill.confirmation_count]], columns = ['uuid', 'skill', 'confirmation_count'])
        person_skills = person_skills.append(person_skill, ignore_index=True)

    persons_skills_df = persons_skills_df.append(person_skills, ignore_index = True)

person_info_path = save_path + 'persons_info_df.csv'
person_educations_path = save_path + 'persons_educations_df.csv'
person_skills_path = save_path + 'persons_skills_df.csv'

persons_info_df.to_csv(person_info_path ,index = False, encoding = 'utf-8')
persons_educations_df.to_csv(person_educations_path, index = False, encoding = 'utf-8')
persons_skills_df.to_csv(person_skills_path, index = False, encoding = 'utf-8')
